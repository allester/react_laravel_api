<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use Dotenv\Validator as DotenvValidator;
use Illuminate\Contracts\Validation\Validator as ContractsValidationValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Validator as ValidationValidator;



class StudentsController extends Controller
{

    public function index()
    {
        $students = Student::all();

        return response()->json([
            'status' => 200,
            'students' => $students
        ]);
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'student_name' => 'required|max:191',
            'student_course' => 'required|max:191',
            'student_email' => 'required|email|max:191',
            'student_phone' => 'required|max:10|min:10'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'validate_err' => $validator->errors()
            ]);
        } else {
            $student = new Student();

            $student->name = $request->input('student_name');
            $student->course = $request->input('student_course');
            $student->email = $request->input('student_email');
            $student->phone = $request->input('student_phone');

            $student->save();

            return response()->json([
                'status' => 200,
                'message' => 'Student Added Successfully'
            ]);
        }
    }

    public function edit($id)
    {
        $student = Student::find($id);

        if ($student) {
            return response()->json([
                'status' => 200,
                'student' => $student
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'No student ID found'
            ]);
        }
    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'student_name' => 'required|max:191',
            'student_course' => 'required|max:191',
            'student_email' => 'required|email|max:191',
            'student_phone' => 'required|max:10|min:10'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'validate_err' => $validator->errors()
            ]);
        } else {
            $student = Student::find($id);

            if ($student) {
                $student->name = $request->input('student_name');
                $student->course = $request->input('student_course');
                $student->email = $request->input('student_email');
                $student->phone = $request->input('student_phone');

                $student->save();

                return response()->json([
                    'status' => 200,
                    'message' => 'Student updated successfully'
                ]);
            } else {
                return response()->json([
                    'status' => 404,
                    'message' => 'No student found'
                ]);
            }
        }
    }

    public function destroy($id)
    {
        $student = Student::find($id);

        if ($student) {
            $student->delete();

            return response()->json([
                'status' => 200,
                'message' => 'Student deleted successfully'
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'Student doesn\'t exist'
            ]);
        }
    }
}
