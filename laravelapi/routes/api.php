<?php

use App\Http\Controllers\API\StudentsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/add-student', [StudentsController::class, 'store']);
Route::get('/students', [StudentsController::class, 'index']);
Route::get('/edit-student/{id}', [StudentsController::class, 'edit']);
Route::put('/update-student/{id}', [StudentsController::class, 'update']);
Route::delete('/delete-student/{id}', [StudentsController::class, 'destroy']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
