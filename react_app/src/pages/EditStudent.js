import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import axios from 'axios'
import swal from 'sweetalert'

class EditStudent extends Component {

    state = {
        student_name: '',
        student_course: '',
        student_email: '',
        student_phone: '',
        error_list: []
    }

    handleInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    async componentDidMount() {
        
        const student_id = this.props.match.params.id
        console.log(student_id);

        const response = await axios.get(`http://localhost:8000/api/edit-student/${student_id}`)
        // console.log(response)

        if (response.data.status === 200) {
            this.setState({
                student_name: response.data.student.name,
                student_course: response.data.student.course,
                student_email: response.data.student.email,
                student_phone: response.data.student.phone
            })
        } else if(response.data.status === 404) {
            swal({
                title: "Warning!",
                text: response.data.message,
                icon: "warning",
                button: "OK!"
            })
            this.props.history.push('/')
        }

    }

    handleSubmit = async (e) => {
        e.preventDefault()

        document.getElementById('updateBtn').disabled = true;
        document.getElementById('updateBtn').innerText = "Updating"
        const student_id = this.props.match.params.id
        const response = await axios.put(`http://localhost:8000/api/update-student/${student_id}`, this.state)

        if (response.data.status === 200) {
            // console.log(response.data.message)

            swal({
                title: "Success!",
                text: response.data.message,
                icon: "success",
                button: "OK!"
            })

            document.getElementById('updateBtn').disabled = false;
            document.getElementById('updateBtn').innerText = 'Update Student';
            this.props.history.push('/');
        } else if(response.data.status === 404){
            swal({
                title: "Warning!",
                text: response.data.message,
                icon: "warning",
                button: "OK!"
            })
            this.props.history.push('/')
        } else {
            this.setState({
                error_list: response.data.validate_err
            })
            document.getElementById('updateBtn').disabled = false;
            document.getElementById('updateBtn').innerText = 'Update Student';
        }
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header">
                                <h4>Edit Student
                                    <Link to={'/'} className="btn btn-primary btn-sm float-end">BACK</Link>
                                </h4>
                            </div>
                            <div className="card-body">
                                <form onSubmit={this.handleSubmit}>
                                    <div className="form-group mb-3">
                                        <label>Student Name:</label>
                                        <input type="text" name="student_name" className="form-control" onChange={this.handleInput} value={this.state.student_name} />
                                        <span className="text-danger">{this.state.error_list.student_name}</span>
                                    </div>

                                    <div className="form-group mb-3">
                                        <label>Student Course:</label>
                                        <input type="text" name="student_course" className="form-control" onChange={this.handleInput} value={this.state.student_course} />
                                        <span className="text-danger">{this.state.error_list.student_course}</span>
                                    </div>

                                    <div className="form-group mb-3">
                                        <label>Student Email:</label>
                                        <input type="email" name="student_email" className="form-control" onChange={this.handleInput} value={this.state.student_email} />
                                        <span className="text-danger">{this.state.error_list.student_email}</span>
                                    </div>

                                    <div className="form-group mb-3">
                                        <label>Student Phone:</label>
                                        <input type="text" name="student_phone" className="form-control" onChange={this.handleInput} value={this.state.student_phone} />
                                        <span className="text-danger">{this.state.error_list.student_phone}</span>
                                    </div>

                                    <button type="submit" className="btn btn-primary btn-block" id="updateBtn">Update Student</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default EditStudent