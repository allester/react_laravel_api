import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import axios from 'axios'
import swal from 'sweetalert'

class AddStudent extends Component {

    state = {
        student_name: '',
        student_email: '',
        student_course: '',
        student_phone: '',
        error_list: [],
    }

    handleInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = async (e) => {
        e.preventDefault()

        const response = await axios.post('http://localhost:8000/api/add-student', this.state)

        if (response.data.status === 200) {
            
            // console.log(response.data.message);

            swal({
                title: "Success!",
                text: response.data.message,
                icon: "success",
                button: "OK"
            })

            this.props.history.push('/')
            
            this.setState({
                student_name: '',
                student_email: '',
                student_course: '',
                student_phone: ''
            })
        } else {
            this.setState({
                error_list: response.data.validate_err,
            })
        }
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header">
                                <h4>Add Student 
                                    <Link to={'/'} className="btn btn-primary btn-sm float-end">BACK</Link>
                                </h4>
                            </div>
                            <div className="card-body">
                                <form onSubmit={this.handleSubmit}>
                                    <div className="form-group mb-3">
                                        <label>Student Name:</label>
                                        <input type="text" name="student_name" className="form-control" onChange={this.handleInput} value={this.state.student_name} />
                                        <span className="text-danger">{this.state.error_list.student_name}</span>
                                    </div>

                                    <div className="form-group mb-3">
                                        <label>Student Course:</label>
                                        <input type="text" name="student_course" className="form-control" onChange={this.handleInput} value={this.state.student_course} />
                                        <span className="text-danger">{this.state.error_list.student_course}</span>
                                    </div>

                                    <div className="form-group mb-3">
                                        <label>Student Email:</label>
                                        <input type="email" name="student_email" className="form-control" onChange={this.handleInput} value={this.state.student_email} />
                                        <span className="text-danger">{this.state.error_list.student_email}</span>
                                    </div>

                                    <div className="form-group mb-3">
                                        <label>Student Phone:</label>
                                        <input type="text" name="student_phone" className="form-control" onChange={this.handleInput} value={this.state.student_phone} />
                                        <span className="text-danger">{this.state.error_list.student_phone}</span>
                                    </div>

                                    <button type="submit" className="btn btn-primary btn-block">Save Student</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AddStudent