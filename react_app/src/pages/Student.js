import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import axios from 'axios'
import swal from 'sweetalert'

class Student extends Component {

    state = {
        students: [],
        loading: true
    }

    async componentDidMount() {

        const response = await axios.get('http://127.0.0.1:8000/api/students');
        
        if (response.data.status === 200) {
            this.setState({
                students: response.data.students,
                loading: false
            })
        }

    }

    handleDelete = async (e, id) => {

        const clickedRecord = e.currentTarget
        clickedRecord.innerText = 'Deleting';

        const response = await axios.delete(`http://127.0.0.1:8000/api/delete-student/${id}`);

        if (response.data.status === 200) {

            clickedRecord.closest('tr').remove();
            // console.log(response.data.message);

            swal({
                title: "Deleted!",
                text: response.data.message,
                icon: "success",
                button: "OK!"
            })
        } else if (response.data.status === 404) {

            swal({
                title: "Warning!",
                text: response.data.message,
                icon: "warning",
                button: "OK"
            })

        } else {
            
        }
    }

    render() {

        let students_HTML_TABLE = "";

        if (this.state.loading) {
            students_HTML_TABLE = <tr><td colSpan="7"><h2>Loading...</h2></td></tr>
        } else {
            students_HTML_TABLE = 
            this.state.students.map((student) => {
                return (
                    <tr key={student.id}>
                        <td>{student.id}</td>
                        <td>{student.name}</td>
                        <td>{student.course}</td>
                        <td>{student.email}</td>
                        <td>{student.phone}</td>
                        <td>
                            <Link to={`edit-student/${student.id}`} className="btn btn-success btn-sm">Edit</Link>
                        </td>
                        <td>
                            <button type="button" onClick={(e) => this.handleDelete(e, student.id)} className="btn btn-danger btn-sm">Delete</button>
                        </td>
                    </tr>
                )
            })
        }

        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-header">
                                <h4>Students Data</h4>
                                <Link to="add-student" className="btn btn-primary btn-sm float-end">Add Student</Link>
                            </div>
                            <div className="card-body">
                                <table className="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Course</th>
                                            <th>Email ID</th>
                                            <th>Phone</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        { students_HTML_TABLE }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Student